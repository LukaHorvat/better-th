module Common (module Common, module X) where

import Prelude as X
import Data.Text as X (Text, pack, unpack)
import Data.Semigroup as X
import Data.List as X
import Debug.Trace as X
import Data.Map as X (Map, (!))
import Data.Function as X
import Data.Functor.Identity as X
import Control.Arrow as X (first, second)
import Control.Applicative as X
import Control.Monad as X

import Text.Regex.PCRE.Heavy
import Text.Regex.PCRE.Light
import Language.Haskell.TH.Quote

re' :: QuasiQuoter
re' = mkRegexQQ [dotall]

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x : _) = Just x
