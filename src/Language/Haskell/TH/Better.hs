{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}
module Language.Haskell.TH.Better where

import Common hiding (many, optional)
import Data.String (IsString)
import Text.ParserCombinators.ReadP
import Control.Effects.Signal
import Control.Effects.Early
import qualified Data.Text as T
import Data.Char hiding (Control)
import Data.Map (fromList)
import Control.Monad.Trans.Class

import Language.Haskell.Meta.Parse
import Language.Haskell.TH.Quote
import Language.Haskell.TH.Syntax hiding (lift)
import Language.Haskell.TH.Ppr (ppr)
import Language.Haskell.TH.PprLib (to_HPJ_Doc)
import Text.PrettyPrint (renderStyle, style, Style(..), Mode(..))

newtype CompiledTemplate = CompiledTemplate { expression :: Exp } deriving (Eq, Ord, Show)

strE :: Text -> CompiledTemplate
strE = CompiledTemplate . AppE (VarE 'pack) . LitE . StringL . unpack

instance Semigroup CompiledTemplate where
    CompiledTemplate exp1 <> CompiledTemplate exp2 =
        CompiledTemplate (AppE (AppE (VarE 'mappend) exp1) exp2)
instance Monoid CompiledTemplate where
    mempty = strE ""
    mappend = (<>)

newtype TemplateControl = TemplateControl
    { resolver :: Text -> [CompiledSegment] -> CompiledTemplate }

data TemplateSegment =
      NoControl { text :: Text }
    | HaskellExpression { expression :: Text }
    | Control { name       :: Text
              , parameters :: Text
              , body       :: [TemplateSegment] }
    deriving (Eq, Ord, Read, Show)

data CompiledSegment =
      CompiledNoControl { text :: Text }
    | CompiledHaskellExpression { expression :: Text }
    | CompiledControl { compiledTemplate :: CompiledTemplate }
    deriving (Eq, Ord, Show)

maybeParse :: ReadP a -> ReadP (Maybe a)
maybeParse p = (Just <$> p) <++ return Nothing

templateParser :: ReadP [TemplateSegment]
templateParser = nonRepeating [control, haskExpression, anythingElse]

maximal :: ReadP a -> ReadP [a]
maximal p = do
    c <- p
    rest <- maximal p <|> return []
    return (c : rest)

nonRepeating :: [ReadP a] -> ReadP [a]
nonRepeating = nonRepeating' . map (, True)
  where
    nonRepeating' :: [(ReadP a, Bool)] -> ReadP [a]
    nonRepeating' ps = do
        (i, a) <- ps & zipWith (\i (p, b) -> ((i, ) <$> p, b)) [0 :: Int ..]
                     & filter snd
                     & map fst
                     & foldl1' (<++)
        let ps' = zipWith (\j p -> if i == j then (p, False) else (p, True)) [0..] (map fst ps)
        rest <- nonRepeating' ps' <++ return []
        return (a : rest)

control :: ReadP TemplateSegment
control = do
    name <- string "{{" *> skipSpaces *> (pack <$> munch1 isAlphaNum) <* skipSpaces
    when (name == "end") pfail
    parameters <- (pack <$> munch (/= '}')) <* string "}}"
    body <- templateParser
    void $ skipSpaces >> string "{{" >> skipSpaces
        >> string "end" >> skipSpaces
        >> string (unpack name)
        >> skipSpaces
        >> string "}}"
    return Control{..}

haskExpression :: ReadP TemplateSegment
haskExpression = do
    void $ char '$'
    let bracketed   = char '{' *> (pack <$> some (satisfy (/= '}'))) <* char '}'
        unbracketed = pack <$> munch isAlphaNum
    expression <- bracketed <++ unbracketed
    return HaskellExpression{..}

anythingElse :: ReadP TemplateSegment
anythingElse = fmap (NoControl . pack) . maximal $ do
    ahead <- look
    case readP_to_S (control <|> haskExpression) ahead of
        [] -> get
        _  -> pfail

parseTemplate :: Text -> [TemplateSegment]
parseTemplate templ = head (map fst (readP_to_S (templateParser <* eof) (unpack templ)))

compileTemplate :: [(Text, TemplateControl)] -> Text -> CompiledTemplate
compileTemplate ctrls = assembleExp . compileTemplate' . parseTemplate
  where
    compileTemplate' = map compileSegment
    compileSegment (NoControl t) = CompiledNoControl t
    compileSegment (HaskellExpression e) = CompiledHaskellExpression e
    compileSegment (Control name params body) = case lookup name ctrls of
        Just (TemplateControl f) -> CompiledControl (f params (compileTemplate' body))
        Nothing                  -> error $ "No control named " <> unpack name

assembleExp :: [CompiledSegment] -> CompiledTemplate
assembleExp = foldMap segmentToExp

segmentToExp :: CompiledSegment -> CompiledTemplate
segmentToExp (CompiledNoControl t) = strE t
segmentToExp (CompiledHaskellExpression e) =
    CompiledTemplate (either error id (parseExp (unpack e)))
segmentToExp (CompiledControl t) = t

metaParse :: (String -> Either String a) -> Text -> a
metaParse p = either error id . p . unpack

toQuoter :: Name -> [(Text, TemplateControl)] -> QuasiQuoter
toQuoter p ctrls = QuasiQuoter
    { quoteDec = error "Not a declataion quoter"
    , quoteExp = return . AppE (AppE (VarE 'metaParse) (VarE p))
               . (expression :: CompiledTemplate -> _) . compileTemplate ctrls . pack
    , quotePat = error "Not a pattern quoter"
    , quoteType = error "Not a type quoter"
    }

dec' :: QuasiQuoter
dec' = toQuoter 'parseDecs []

exp' :: QuasiQuoter
exp' = toQuoter 'parseExp []

embedExp :: Exp -> Text
embedExp e = pack (renderStyle (style { mode = OneLineMode }) (to_HPJ_Doc (ppr e)))

--
-- data ParseError = MismatchedControls ControlName ControlName | SyntaxError Text
--     deriving (Eq, Ord, Read, Show)
--
-- resolveControls :: forall m. Throws ParseError m
--                 => Map ControlName TemplateControl -> Text -> m Text
-- resolveControls ts = resolveControls' "" "" 0
--   where
--     resolveControls' :: ControlName -> ControlParameters -> Int -> Text -> m Text
--     resolveControls' name' params spaces templ =
--         case findControl templ of
--             Just (section, _, "end", ControlParameters name, after)
--                 | ControlName name == name' -> return $
--                         ( section & ControlBody
--                                   & resolver (ts ! name') params )
--                         <> after
--                 | otherwise -> throwSignal (MismatchedControls (ControlName name) name')
--             Just (before, _, name, params', after) ->
--                 resolveControls' name' params spaces =<<
--                     (before <>) <$> resolveControls' name params' spaces after
--             Nothing -> return templ
--
-- appendExp :: Exp -> Exp -> Exp
-- appendExp e1 = AppE (AppE (VarE 'mappend) e1)
--
--
-- newtype Template = Template Text deriving (Eq, Ord, Read, Show)
--
-- resolveNames :: Text -> Q Exp
-- resolveNames = fmap (AppE (ConE 'Template)) . resolveNames'
--   where
--     resolveNames' txt = case scan [re'|(.*)\$(\S*)(.*)|] txt of
--         (_, [before, name, after]) : _ -> handleEarly $ do
--             valName <- lift (lookupValueName (unpack name))
--                    >>= ifNothingDo (fail ("Name " <> unpack name <> " not in scope"))
--             rest <- lift (resolveNames' after)
--             return (strE before `appendExp` VarE valName `appendExp` rest)
--         _ -> return (strE txt)
--
-- runTemplate :: Throws ParseError m => [(Text, TemplateControl)] -> Template -> m [Dec]
-- runTemplate ctrls (Template txt) = do
--     resolved <- resolveControls (fromList (fmap (first ControlName) ctrls)) txt
--     either (throwSignal . SyntaxError . pack) return (parseDecs (unpack resolved))
--
-- runTemplateQ :: Monad m => [(Text, TemplateControl)] -> Template -> m [Dec]
-- runTemplateQ ctrls = handleWithFail . runTemplate ctrls
--
-- handleWithFail :: Monad m => ExceptT _ Identity c -> m c
-- handleWithFail = either (fail . unpack) return . runIdentity . showAllExceptions
--
-- dec :: QuasiQuoter
-- dec = QuasiQuoter
--     { quoteDec = error "Not a declataion quoter"
--     , quoteExp = resolveNames . pack
--     , quotePat = error "Not a pattern quoter"
--     , quoteType = error "Not a type quoter"
--     }
--
-- shiftOne :: TemplateControl
-- shiftOne = TemplateControl $ \_ (ControlBody t) ->
--     T.map (\c -> if isAlphaNum c then succ c else c) t
--
-- testText :: Text
-- testText =
--     "1\n\
--     \{{ one }}\n\
--     \1\n\
--     \{{ one }}\n\
--     \1\n\
--     \{{ end one }}\n\
--     \1\n\
--     \{{ end one }}\n\
--     \1"
