{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wno-partial-type-signatures #-}
module Language.Haskell.TH.Better.Controls where

import Common

import qualified Data.Text as Text

import Language.Haskell.TH.Better
import Language.Haskell.TH.Syntax
import Language.Haskell.TH.Quote
import Language.Haskell.Meta hiding (toExp)

toExp :: [CompiledSegment] -> Exp
toExp = (expression :: CompiledTemplate -> _) . assembleExp

forControl :: TemplateControl
forControl = TemplateControl (\params body -> case Text.splitOn " " params of
    var : "in" : (Text.concat -> vars) ->
        CompiledTemplate [exp'|foldMap (\$var -> ${embedExp (toExp body)}) $vars|]
    )

dec'' :: QuasiQuoter
dec'' = toQuoter 'parseDecs [("for", forControl)]
