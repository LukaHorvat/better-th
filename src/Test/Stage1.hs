{-# LANGUAGE QuasiQuotes, TemplateHaskell #-}
module Test.Stage1 where

import Common
import GHC.Base
import Data.Text
import Language.Haskell.TH.Better.Controls

tshow :: Show a => a -> Text
tshow = pack . show

gen n = [dec''|
    {{ for a in [1..n] }}
    data A${tshow a} = A${tshow a}
    {{ end for }}
    |]
